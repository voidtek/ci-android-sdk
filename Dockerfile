ARG OPENJDK_VERSION=8u222-jdk

FROM openjdk:${OPENJDK_VERSION}

ARG ANDROID_COMPILE_SDK=29
ARG ANDROID_BUILD_TOOLS=29.0.2
ARG ANDROID_SDK_TOOLS=4333796

RUN mkdir $HOME/.android \
  && echo 'count=0' > $HOME/.android/repositories.cfg

RUN apt-get --quiet update --yes \
  && apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip \
  && unzip -d android-sdk-linux android-sdk.zip \
  && rm android-sdk.zip

RUN export ANDROID_HOME=$PWD/android-sdk-linux \
  && export PATH=$PATH:$ANDROID_HOME/platform-tools/

RUN echo y | android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null \
  && echo y | android-sdk-linux/tools/bin/sdkmanager "platform-tools" >/dev/null \
  && echo y | android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null

RUN yes | android-sdk-linux/tools/bin/sdkmanager --licenses

RUN apt-get autoclean -y \
  && apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
